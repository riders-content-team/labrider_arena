#include "HeatMaze.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(HeatMazePlugin)


// Constructor
HeatMazePlugin::HeatMazePlugin() : WorldPlugin()
{
}


// Deconstructor
HeatMazePlugin::~HeatMazePlugin()
{
}


// Runs once on initialization
void HeatMazePlugin::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
    //Start Condition
    this->crate_num = 0;
    this->number_num = 0;

    this->world = _world;
    this->LoadMaze(_sdf);


    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "spawn_model_service_node",
                  ros::init_options::NoSigintHandler);
    }

    // Create our ROS node. This acts in a similar manner to
    // the Gazebo node
    this->rosNode.reset(new ros::NodeHandle());

    this->spawnService = this->rosNode->advertiseService<labrider_arena::HeatStatus::Request, labrider_arena::HeatStatus::Response>
        ("get_surrounding_heats", std::bind(&HeatMazePlugin::GetSurroundingHeats, this, std::placeholders::_1, std::placeholders::_2));

    this->clearService = this->rosNode->advertiseService<std_srvs::Trigger::Request, std_srvs::Trigger::Response>
        ("clear_all_heat", std::bind(&HeatMazePlugin::ClearAllHeat, this, std::placeholders::_1, std::placeholders::_2));

    this->heatMapService = this->rosNode->advertiseService<std_srvs::Trigger::Request, std_srvs::Trigger::Response>
        ("return_all_heat", std::bind(&HeatMazePlugin::ReturnHeats, this, std::placeholders::_1, std::placeholders::_2));

    this->singleSpawnService = this->rosNode->advertiseService<labrider_arena::SingleHeat::Request, labrider_arena::SingleHeat::Response>
        ("process_single_heat", std::bind(&HeatMazePlugin::ProcessSingleHeat, this, std::placeholders::_1, std::placeholders::_2));

    this->singleCleanService = this->rosNode->advertiseService<labrider_arena::SingleHeat::Request, labrider_arena::SingleHeat::Response>
        ("clean_single_tile", std::bind(&HeatMazePlugin::CleanSingleTile, this, std::placeholders::_1, std::placeholders::_2));

    // Connect to the world update event.
    this->worldConnection = event::Events::ConnectWorldUpdateBegin(
            std::bind(&HeatMazePlugin::Spin, this));

    // Connect to the world reset event.
    this->resetConnection = event::Events::ConnectWorldReset(
            std::bind(&HeatMazePlugin::OnReset, this));
}

void HeatMazePlugin::Spin()
{
  if (ros::ok())
  {
    ros::spinOnce();
  }
}

void HeatMazePlugin::OnReset()
{
    for (std::vector<std::string>::iterator ni = this->number_models.begin(); ni != this->number_models.end(); ++ni)
    {
        this->world->RemoveModel(*ni);
    }

    this->number_models.clear();
    this->tile_numbers.clear();

    for (int i = 0; i < this->y_dim; i++)
    {
        for (int j = 0; j < this->x_dim; j++)
        {
            std::vector<std::string> empty_string_vector;
            this->tile_numbers.push_back(empty_string_vector);
        }
    }


    //Start Condition
    this->number_num = 0;

    std::memcpy(this->HeatMap, this->Maze, this->x_dim * this->y_dim * sizeof(int));
}


void HeatMazePlugin::LoadMaze( sdf::ElementPtr sdf)
{
    if (sdf->HasElement("maze_dimensition"))
    {
        std::string dimensions = sdf->Get<std::string>("maze_dimensition");

        std::string delimiter = " ";
        std::string x_dim_string = dimensions.substr(0, dimensions.find(delimiter));
        std::string y_dim_string = dimensions.substr(dimensions.find(delimiter) + 1, dimensions.length() - 1);

        this->x_dim = std::stoi(x_dim_string);
        this->y_dim = std::stoi(y_dim_string);
        
        this->Maze = (int*)std::malloc(this->x_dim * this->y_dim * sizeof(int));
        this->HeatMap = (int*)std::malloc(this->x_dim * this->y_dim * sizeof(int));
    }


    if (sdf->HasElement("origin"))
    {
        std::string origin_string = sdf->Get<std::string>("origin");
        std::string delimiter = " ";

        std::string origin_x_string = origin_string.substr(0, origin_string.find(delimiter));
        std::string origin_y_string = origin_string.substr(origin_string.find(delimiter) + 1, origin_string.length() - 1);

        this->origin_x = std::stod(origin_x_string);
        this->origin_y = std::stod(origin_y_string);
    }


    if (sdf->HasElement("rows_in_binary"))
    {
        std::string total_rows = sdf->Get<std::string>("rows_in_binary");

        for (int i = 0; i < this->y_dim; i++)
        {
            std::string binary_row = total_rows.substr(i * (this->x_dim + 1), this->x_dim + 1);

            for (int j = 0; j < this->x_dim; j++)
            {
                std::vector<std::string> empty_string_vector;
                this->tile_numbers.push_back(empty_string_vector);

                if (binary_row[j] == '1')
                {
                  // Don't spawn - part of world file now
                //  this->SpawnModel("crate", "crate_" + std::to_string(this->crate_num), (double)j + origin_x, (double)i + origin_y, 0);
                  this->crate_num++;
                  this->Maze[this->x_dim * i + j] = -1;
                  
                  //this->SpawnNumber(-1, (double)j, (double)i, 0.48);
                }
                else
                {
                    this->Maze[this->x_dim * i + j] = 0;
                    //this->SpawnNumber(0, (double)j, (double)i, 0.1);
                }                
            }
        }
        std::memcpy(this->HeatMap, this->Maze, this->x_dim * this->y_dim * sizeof(int));
    }
}

void HeatMazePlugin::SpawnModel(std::string model_name, std::string spawn_name, double pose_x, double pose_y, double pose_z)
{
    std::string pose_x_str = std::to_string(pose_x);
    std::string pose_y_str = std::to_string(pose_y);
    std::string pose_z_str = std::to_string(pose_z);

    std::string sdf_file_string = "<?xml version='1.0' ?> "
                                  "<sdf version='1.5'>"
                                  "<include>"
                                  "<pose>" + pose_x_str + " " + pose_y_str + pose_z_str + " 0 0 0" + "</pose>"
                                  "<name> " + spawn_name + "</name>"
                                  "<uri>model://" + model_name + "</uri>"
                                  "</include>"
                                  "</sdf>";

    this->world->InsertModelString(sdf_file_string);
}


void HeatMazePlugin::SpawnNumber(int number, double relative_x, double relative_y, double pose_z)
{
    double number_height = 0.1;
    double number_distance = 0.4;

    double pose_x = relative_x + this->origin_x;
    double pose_y = relative_y + this->origin_y;

    if(number >= 0)
    {
        if(number >= 10)
        {
            int primary_number = number % 10;
            int secondary_number = number / 10;

            std::string primary_model_name = "number_" + std::to_string(primary_number);
            std::string secondary_model_name = "number_" + std::to_string(secondary_number);

            std::string primary_spawn_name = primary_model_name + "_" + std::to_string(number_num);
            this->number_num++;
            std::string secondary_spawn_name = secondary_model_name + "_" + std::to_string(number_num);
            this->number_num++;

            this->number_models.push_back(primary_spawn_name);
            this->number_models.push_back(secondary_spawn_name);
        
            this->tile_numbers[relative_y * this->x_dim + relative_x].push_back(primary_spawn_name);
            this->tile_numbers[relative_y * this->x_dim + relative_x].push_back(secondary_spawn_name);

            this->SpawnModel(primary_model_name, primary_spawn_name, pose_x + number_distance/2, pose_y, pose_z);
            this->SpawnModel(secondary_model_name, secondary_spawn_name, pose_x - number_distance/2, pose_y, pose_z);

        }
        else
        {
            std::string model_name = "number_" + std::to_string(number);
            std::string spawn_name = model_name + "_" + std::to_string(number_num);
            this->number_num++;

            this->number_models.push_back(spawn_name);

            this->tile_numbers[relative_y * this->x_dim + relative_x].push_back(spawn_name);

            this->SpawnModel(model_name, spawn_name, pose_x, pose_y, pose_z);
        }        
    }
    else
    {
        std::string model_name = "number_" + std::to_string(number);
        std::string spawn_name = model_name + "_" + std::to_string(number_num);
        this->number_num++;

        this->number_models.push_back(spawn_name);

        this->tile_numbers[relative_y * this->x_dim + relative_x].push_back(spawn_name);

        this->SpawnModel(model_name, spawn_name, pose_x, pose_y, 0.48);

        /*        
        if(number <= -10)
        {
            int primary_number = -(number % 10);
            int secondary_number = -(number / 10);

            std::string dash_model_name = "number_dash";
            std::string primary_model_name = "number_" + std::to_string(primary_number);
            std::string secondary_model_name = "number_" + std::to_string(secondary_number);

            std::string dash_spawn_name = dash_model_name + "_" + std::to_string(number_num);
            this->number_num++;
            std::string primary_spawn_name = primary_model_name + "_" + std::to_string(number_num);
            this->number_num++;
            std::string secondary_spawn_name = secondary_model_name + "_" + std::to_string(number_num);
            this->number_num++;

            this->number_models.push_back(dash_spawn_name);
            this->number_models.push_back(primary_spawn_name);
            this->number_models.push_back(secondary_spawn_name);

            this->tile_numbers[relative_y * this->x_dim + relative_x].push_back(dash_spawn_name);
            this->tile_numbers[relative_y * this->x_dim + relative_x].push_back(primary_spawn_name);
            this->tile_numbers[relative_y * this->x_dim + relative_x].push_back(secondary_spawn_name);
            

            this->SpawnModel(primary_model_name, primary_spawn_name, pose_x + 0.3, pose_y, number_height);
            this->SpawnModel(secondary_model_name, secondary_spawn_name, pose_x - 0.1, pose_y, number_height);
            this->SpawnModel(dash_model_name, dash_spawn_name, pose_x - 0.3, pose_y, number_height);

        }
        else
        {
            std::string dash_model_name = "number_dash";
            std::string model_name = "number_" + std::to_string(-number);
            
            std::string dash_spawn_name = dash_model_name + "_" + std::to_string(number_num);
            this->number_num++;
            std::string spawn_name = model_name + "_" + std::to_string(number_num);
            this->number_num++;

            this->number_models.push_back(dash_spawn_name);
            this->number_models.push_back(spawn_name);

            this->tile_numbers[relative_y * this->x_dim + relative_x].push_back(dash_spawn_name);
            this->tile_numbers[relative_y * this->x_dim + relative_x].push_back(spawn_name);

            this->SpawnModel(dash_model_name, dash_spawn_name, pose_x - 0.15, pose_y, 0.47);
            this->SpawnModel(model_name, spawn_name, pose_x + 0.15, pose_y, 0.47);
        }
        */
    }
}


bool HeatMazePlugin::GetSurroundingHeats(labrider_arena::HeatStatus::Request &req, labrider_arena::HeatStatus::Response &res)
{
    double robot_pose_x = req.x;
    double robot_pose_y = req.y;

    int relative_pose_y = robot_pose_y - this->origin_y;
    int relative_pose_x = robot_pose_x - this->origin_x;

    res.center_heat = this->HeatMap[this->x_dim * (relative_pose_y) + relative_pose_x];
    res.north_heat = this->HeatMap[this->x_dim * (relative_pose_y + 1) + relative_pose_x];
    res.east_heat = this->HeatMap[this->x_dim * (relative_pose_y) + relative_pose_x + 1];
    res.south_heat = this->HeatMap[this->x_dim * (relative_pose_y - 1) + relative_pose_x];
    res.west_heat = this->HeatMap[this->x_dim * (relative_pose_y) + relative_pose_x - 1];

    return true;
}


bool HeatMazePlugin::ClearAllHeat(std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
{
  for (std::vector<std::string>::iterator ni = this->number_models.begin();
    ni != this->number_models.end(); ++ni)
  {
      this->world->RemoveModel(*ni);
  }
  this->number_models.clear();

  //Start Condition
  this->number_num = 0;

  std::memcpy(this->HeatMap, this->Maze, this->x_dim * this->x_dim * sizeof(int));

  res.success = true;
  res.message = "";

  return true;
}

bool HeatMazePlugin::ReturnHeats(std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
{
    std::string all_heats;

    for (int i=0; i < this->y_dim; i++)
    {
        for (int j=0; j<this->x_dim; j++)
        {
            all_heats += std::to_string(this->HeatMap[i*this->x_dim + j]) + ":";
        }
    }

    res.success = true;
    res.message = all_heats;

    return true;
}

bool HeatMazePlugin::ProcessSingleHeat(labrider_arena::SingleHeat::Request &req, labrider_arena::SingleHeat::Response &res)
{
    double relative_pose_x = req.x;
    double relative_pose_y = req.y;

    std::string number_model = req.number_name;
    int number = std::stoi(number_model);

    this->SpawnNumber(number, req.x, req.y, 0.1);

    this->HeatMap[this->x_dim * ((int)relative_pose_y) + (int)relative_pose_x] = number;

    res.center_heat = this->HeatMap[this->x_dim * ((int)relative_pose_y) + (int)relative_pose_x];

    return true;
}

bool HeatMazePlugin::CleanSingleTile(labrider_arena::SingleHeat::Request &req, labrider_arena::SingleHeat::Response &res)
{
    std::vector<std::string> & numbers_in_tile = this->tile_numbers[req.y * this->x_dim + req.x];

    for (std::vector<std::string>::iterator ni = numbers_in_tile.begin(); ni != numbers_in_tile.end(); ++ni)
    {
        // gazebo will allow RemoveModel to execute before the Spawn has actually been executed.
        // Then we will miss it and be left with a lingering number.
        // This loop is not the best way to handle this. Need a safer design.
        const std::string & model_name = *ni;
        gazebo::physics::ModelPtr modelptr = this->world->ModelByName(model_name);
        while(modelptr == NULL) {
            Spin();
            modelptr = this->world->ModelByName(model_name);
        }

        this->world->RemoveModel(model_name);
    }

    numbers_in_tile.clear();

    this->HeatMap[this->x_dim*(int)req.y + (int)req.x] = 0;

    return true;
}
